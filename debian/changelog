jboss-logging (3.5.0-2+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:31:57 +0530

jboss-logging (3.5.0-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:46:22 +0000

jboss-logging (3.5.0-2) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.1.
  * Fix FTBFS with Java 17. (Closes: #1012082)

 -- Markus Koschany <apo@debian.org>  Tue, 23 Aug 2022 13:42:25 +0200

jboss-logging (3.5.0-1) unstable; urgency=medium

  * New upstream version 3.5.0.

 -- Markus Koschany <apo@debian.org>  Wed, 04 May 2022 12:40:51 +0200

jboss-logging (3.4.3-1) unstable; urgency=medium

  * New upstream version 3.4.3.

 -- Markus Koschany <apo@debian.org>  Sun, 06 Feb 2022 22:18:22 +0100

jboss-logging (3.4.2-1) unstable; urgency=medium

  * New upstream version 3.4.2.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.6.0.
  * Build-depend on liblogback-java.

 -- Markus Koschany <apo@debian.org>  Sat, 25 Sep 2021 23:01:27 +0200

jboss-logging (3.4.1-1+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:19 +0530

jboss-logging (3.4.1-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 00:01:32 +0000

jboss-logging (3.4.1-1) unstable; urgency=medium

  * New upstream version 3.4.1.

 -- Markus Koschany <apo@debian.org>  Mon, 12 Aug 2019 00:45:34 +0200

jboss-logging (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0.
  * Switch to debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.0.
  * Remove get-orig-source target.
  * Use canonical VCS URI.
  * Drop disable-apiviz-plugin.patch.
  * Drop SLF4j-1.7.patch. No longer needed.

 -- Markus Koschany <apo@debian.org>  Sat, 20 Jul 2019 03:21:28 +0200

jboss-logging (3.3.2-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 17 Feb 2021 00:01:25 +0000

jboss-logging (3.3.2-1) unstable; urgency=medium

  * New upstream version 3.3.2.
  * Use compat level 11.
  * Declare compliance with Debian Policy 4.1.3.

 -- Markus Koschany <apo@debian.org>  Thu, 15 Feb 2018 17:52:29 +0100

jboss-logging (3.3.1-1) unstable; urgency=medium

  * New upstream version 3.3.1.
  * Declare compliance with Debian Policy 4.0.0.
  * Update copyright years.

 -- Markus Koschany <apo@debian.org>  Tue, 20 Jun 2017 12:23:27 +0200

jboss-logging (3.3.0-2) unstable; urgency=medium

  * Switch to compat level 10.
  * Switch from cdbs to dh sequencer.
  * Vcs-Git: Use https.
  * Update my email address.
  * Declare compliance with Debian Policy 3.9.8.

 -- Markus Koschany <apo@debian.org>  Sat, 24 Dec 2016 00:52:05 +0100

jboss-logging (3.3.0-1) unstable; urgency=medium

  * Imported Upstream version 3.3.0.
  * Rebase and update SLF4j-1.7.patch for new release.

 -- Markus Koschany <apo@gambaru.de>  Sun, 31 May 2015 13:51:28 +0200

jboss-logging (3.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #779659)

 -- Markus Koschany <apo@gambaru.de>  Tue, 12 May 2015 17:00:22 +0200
